<?php

$serveur = 'localhost';
$dbname = 'contact';
$user = 'root';
$pass='';

try{
    $dbco = new PDO("mysql:host=$serveur;dbname=$dbname;charset=utf8",$user,$pass);
    $dbco->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}
catch (Exception $e){
    print 'pas de connexion possible';
    die('Erreur : ' . $e->getMessage());
}

    $reponse = $dbco->prepare("SELECT t_email.Email, t_personne.prenom, t_personne.nom, t_msg.msg, t_msg.etat, t_msg.id_msg
    FROM t_email
    INNER JOIN t_personne ON t_email.id_email = t_personne.id_email
    INNER JOIN t_msg ON t_email.id_email = t_msg.id_email");

    $reponse->execute();

    
    while ($donnees = $reponse->fetch())
    {
    // Affiche les données
    ?>
        <p>
            <strong><?php print $donnees['prenom']; ?> 
            <?php print $donnees['nom']; ?>
            </strong> vous a envoyé un message avec l'adresse mail suivante : 
            <?php print $donnees['Email']; ?><br>
            <strong>Le message</strong> : <em><?php print $donnees['msg'];
            ?></em><br><br>
            <strong>État du message</strong> : <?php print $donnees['etat']
            ?><br>
        </p>
        <form action="traitement.php" method="post" name="formEtat">
        <p>Voulez-vous changer l'état du message ? (si non, ne rien cocher) : </p>
            <div>
                <input type="radio" name="choisirEtat" value="A traiter">A traiter
            </div>
            <div>
                <input type="radio" name="choisirEtat" value=" A relancer">A relancer
            </div>
            <div>
                <input type="radio" name="choisirEtat" value="En attente">En attente
            </div>
            <div>
                <input type="radio" name="choisirEtat" value="RDV pris">RDV pris
            </div>
            <div>
                <input type="radio" name="choisirEtat" value="Sans suite">Sans suite
            </div>
            <br>
            <div>
            <input type="hidden" name="id_msg" value="<?php print $donnees['id_msg'] ?>">
                <button type="submit">Envoyer</button>
            </div>
            </form>
            <br><br><br>
        <?php

    }


try{
        $req = $dbco->prepare("UPDATE t_msg 
                                SET etat = :etat WHERE id_msg = :id_req_msg");
        $req->bindParam(':etat',$_POST['choisirEtat'], PDO::PARAM_STR);
        $req->bindParam(':id_req_msg', $_POST['id_msg'], PDO::PARAM_STR);

        $req->execute();
}
    catch(Exception $ExceptionRaised) {
        print "Changement non pris en compte" . $ExceptionRaised->getMessage();
    
}

?>