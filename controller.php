<?php

$serveur = "localhost";
$dbname = "contact";
$user = "root";
$pass ="";

$prenom = valid_donnees($_POST['prenom']);
$nom = valid_donnees($_POST['nom']);
$commentaire = valid_donnees($_POST['commentaire']);
$commentaire = filter_var($_POST['commentaire'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$mail =valid_donnees($_POST['mail']);

function valid_donnees($donnees) {
    $donnees = trim($donnees);
    $donnees = stripslashes($donnees);
    $donnees = htmlspecialchars($donnees);
    $donnees = htmlentities($donnees);
    $donnees = strip_tags($donnees);
    return $donnees;
}

if (!empty($prenom)
&& strlen($prenom) <= 20
&& preg_match("/^[A-Za-z '-]+$/", $prenom)
&& !empty($nom)
&&strlen($nom) <= 20
&& preg_match("/^[A-Za-z '-]+$/", $nom)
&& !empty($commentaire)
&& strlen($commentaire) <= 500
&& !empty($mail)
&& preg_match(" /^.+@.+\.[a-zA-Z]{2,}$/ " , $mail)
&& filter_var($mail, FILTER_VALIDATE_EMAIL)) {

    try{
        $dbco = new PDO("mysql:host=$serveur;dbname=$dbname",$user,$pass);
        $dbco->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        if(!empty($prenom) && !empty($nom) && !empty($commentaire) && !empty($mail)) {

            $contact = $dbco->prepare("
            INSERT INTO t_email(Email)
            VALUES(:email)");

            $contact->bindParam(':email',$mail);
            $contact->execute();

            $messageId = $dbco->prepare("SET @ID_EMAIL =(SELECT id_email FROM t_email WHERE Email = :email)");
            $messageId->bindParam(':email', $mail);
            $messageId->execute();

            $sth = $dbco->prepare("
                    INSERT INTO t_personne(prenom, nom, id_email)
                    VALUES(:prenom, :nom, @ID_EMAIL)");

            $sth->bindParam(':prenom',$prenom);
            $sth->bindParam(':nom',$nom);
           // $sth->bindParam(':commentaire',$commentaire);
            //$sth->bindParam(':mail',$mail);
            $sth->execute();

            $message= $dbco->prepare("
            INSERT INTO t_msg(msg, id_email)
            VALUES(:msg, @ID_EMAIL)");

            $message->bindParam(':msg',$commentaire);
            $message->execute();
           
        }

        header("Location:form-merci.html");

    }
    catch(PDOException $e){
        echo 'Erreur : ' . $e->getMessage();
    }

    $to = "tiba26@hotmail.fr";
    $subject = "Message de la part de $email";
    $mess = $msg;
    $headers = "From: $email" .PHP_EOL . "Content-type: text/html; charset=utf-8" .PHP_EOL . "X-mailer: PHP/" . phpversion();
}


?>