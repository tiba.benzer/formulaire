<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire en PHP/MySQL</title>
</head>
<body>
    <form action="controller.php" method="post">
        <p>
            <label for="prenom"> Prénom : </label> : <input type="text" name="prenom" id="prenom" 
            required pattern="^[A-Za-z '-]+$" maxlength="20"> <br />
      
            <label for="nom"> Nom : </label> : <input type="text" name="nom" id="nom" 
            required pattern="^[A-Za-z '-]+$" maxlength="20"> <br />
        
            <label for="mel"> Email : </label> : <input type="email" name="mail" id="mail" 
            > <br />
      
            <label for="commentaire"> Commentaire : </label> : <textarea name="commentaire" id="commentaire" 
             maxlength="255"></textarea> <br />

        </p>

        <input type="submit" name="submit" value="Envoyer" />

    </form>
</body>
</html>
